require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module SampleApp
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # From https://gist.github.com/shad1w/75e7ebbbdc5382719cd06f56bbe6acd0
    config.assets.prefix = "#{ENV['RAILS_TUTORIAL_RELATIVE_URL_ROOT']}#{config.assets.prefix}"
  end
end
