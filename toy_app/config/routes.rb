Rails.application.routes.draw do
  scope ENV['RAILS_TUTORIAL_RELATIVE_URL_ROOT'] || '/' do
    resources :users
    root 'application#hello'
  end
end